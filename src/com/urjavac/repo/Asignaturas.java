package com.urjavac.repo;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gcm.GCMRegistrar;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class Asignaturas extends ListActivity {

	private SharedPreferences preferencias;
	private SharedPreferences.Editor editor;

	private SharedPreferences config;
	private SharedPreferences.Editor editConfig;
	
	private ProgressDialog pDialog;

	private String admin;
	Almacen_asignaturas almacen;
	Almacen_uploaders uploaders = new Almacen_uploaders();
	static Boolean inicio = true;
	String usuario;

	/*
	 * M�todo OnCreate: Obtenemos los string que se pasaron en el Intent (Admin
	 * y Usuario) para utilizarlos despu�s. Se crea el Toast Eliminamos todos
	 * los elementos del Almacen de asignaturas para que no se dupliquen.
	 * Comprobamos la conexi�n a internet y cambiamos Inicio a false para que la
	 * proxima vez se lea desde archivo y no desde la BBDD.
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_asignaturas);
		almacen = new Almacen_asignaturas();
		Bundle bundle = getIntent().getExtras();

		admin = bundle.getString("admin");
		usuario = bundle.getString("usuario");
		/************** NOTIFICACIONES *******************/
		config = getSharedPreferences("configuracion", Context.MODE_PRIVATE);
		String nuevo = config.getString("recibe_noti", "nuevo");
		if(nuevo.equals("nuevo")){ //Es un usuario nuevo y hay que activar las notificaciones. Si obtenemos "si" o "no" no tocamos nada.
			editConfig = config.edit();
			editConfig.putString("usuario", usuario);
			editConfig.putString("recibe_noti", "si");
			editConfig.commit();
			GCMRegistrar.register(Asignaturas.this, "786938045651");
		}
		
		if (inicio) {
			inicio = bundle.getBoolean("inicio");

			if (admin.equals("si")) {
				Toast.makeText(this, "Bienvenido Administrador: " + usuario,
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, "Bienvenido usuario: " + usuario,
						Toast.LENGTH_LONG).show();
			}
		}
		Almacen_asignaturas.getAlmacen().removeAllElements();
		if (inicio) {
			if (isOnline()) {
				new AsyncAsig().execute(admin);
			} else {
				Toast.makeText(this, "Error. Compruebe su conexi�n a Internet",
						Toast.LENGTH_LONG).show();
			}

		} else {
			Almacen_asignaturas.leerAlmacen();
			setListAdapter(new MiAdaptadorAsignaturas(Asignaturas.this,
					Almacen_asignaturas.getAlmacen()));

		}
		inicio = false;
	}

	/*
	 * Asignaci�n del XML del men� dependiendo si es o no Admin.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		if (admin.equals("si")) {
			getMenuInflater().inflate(R.menu.asignaturas_admin, menu);
		} else {
			getMenuInflater().inflate(R.menu.asignaturas_user, menu);
		}
		return true;
	}

	/*
	 * Comprobar si est� online
	 */
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}

		return false;
	}

	/*
	 * Metodo listener de cada elemento de la lista
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (Almacen_asignaturas.getAlmacen().elementAt(position).getRecursos() > 0) {
			Intent i = new Intent(Asignaturas.this, Recursos.class);
			i.putExtra("asignatura", Almacen_asignaturas.getAlmacen()
					.elementAt(position).getNombre());
			i.putExtra("id",
					Almacen_asignaturas.getAlmacen().elementAt(position)
							.getId());
			i.putExtra("admin", admin);
			i.putExtra("recursos",
					Almacen_asignaturas.getAlmacen().elementAt(position)
							.getRecursos());
			startActivity(i);
		} else {
			Toast.makeText(this,
					"No hay ning�n recurso publicado",
					Toast.LENGTH_SHORT).show();
		}
	}

	/*
	 * Comportamiento cuando se elige una opci�n del men�.
	 */

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == R.id.cerrar_sesion
				|| item.getItemId() == R.id.cerrar_sesion_user) {
			cerrarSesion();
		} else if (item.getItemId() == R.id.configuracion) {
			Intent i = new Intent(Asignaturas.this, Notificaciones.class);
			i.putExtra("usuario", usuario);
			startActivity(i);

			// INICIO MENU ADMIN
		} else if (item.getItemId() == R.id.add_apuntes) {
			Intent i = new Intent(Asignaturas.this, AddRecurso.class);
			i.putExtra("tipo", 1);
			startActivity(i);
		} else if (item.getItemId() == R.id.add_examen) {
			Intent i = new Intent(Asignaturas.this, AddRecurso.class);
			i.putExtra("tipo", 2);
			startActivity(i);
		} else if (item.getItemId() == R.id.add_sol_examen) {
			Intent i = new Intent(Asignaturas.this, AddRecurso.class);
			i.putExtra("tipo", 3);
			startActivity(i);
		} else if (item.getItemId() == R.id.add_practica) {
			Intent i = new Intent(Asignaturas.this, AddRecurso.class);
			i.putExtra("tipo", 4);
			startActivity(i);
		} else if (item.getItemId() == R.id.add_sol_practica) {
			Intent i = new Intent(Asignaturas.this, AddRecurso.class);
			i.putExtra("tipo", 5);
			startActivity(i);
		} else if (item.getItemId() == R.id.b_addAsignatura) {
			Intent i = new Intent(Asignaturas.this, AddAsignatura.class);
			startActivity(i);
			// INICIO MENU DE USUARIOS
		} else if (item.getItemId() == R.id.envio_apuntes) {
			Intent i = new Intent(Asignaturas.this, EnviarRecurso.class);
			i.putExtra("tipo", 1);
			i.putExtra("usuario", usuario);
			startActivity(i);
		} else if (item.getItemId() == R.id.envio_examen) {
			Intent i = new Intent(Asignaturas.this, EnviarRecurso.class);
			i.putExtra("tipo", 2);
			i.putExtra("usuario", usuario);
			startActivity(i);
		} else if (item.getItemId() == R.id.envio_sol_examen) {
			Intent i = new Intent(Asignaturas.this, EnviarRecurso.class);
			i.putExtra("tipo", 3);
			i.putExtra("usuario", usuario);
			startActivity(i);
		} else if (item.getItemId() == R.id.envio_practica) {
			Intent i = new Intent(Asignaturas.this, EnviarRecurso.class);
			i.putExtra("tipo", 4);
			i.putExtra("usuario", usuario);
			startActivity(i);
		} else if (item.getItemId() == R.id.envio_sol_practica) {
			Intent i = new Intent(Asignaturas.this, EnviarRecurso.class);
			i.putExtra("tipo", 5);
			i.putExtra("usuario", usuario);
			startActivity(i);
		}
		return super.onMenuItemSelected(featureId, item);
	}

	/*
	 * Cerrar Sesi�n consiste en eliminar de las preferencias el usuario y el
	 * pass y adem�s abrir el MainActivity.class
	 */
	public void cerrarSesion() {
		preferencias = getSharedPreferences("Login", Context.MODE_PRIVATE);
		editor = preferencias.edit();
		editor.putString("Nombre", "");
		editor.putString("Password", "");
		editor.commit();
		Intent i = new Intent(this, MainActivity.class);
		finish();
		startActivity(i);
	}

	/*
	 * S�lo lo utilizo para iniciar el Analytics.
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	/*
	 * Igual para pararlo.
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.

	}

	public class AsyncAsig extends AsyncTask<String, String, String> {

		String admin;

		/*
		 * Abrir el ProgressDialog
		 */
		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(Asignaturas.this);
			pDialog.setMessage("Descargando asignaturas...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/*
		 * B�sicamente: descargo las asignaturas si es usuario normal y si es
		 * admin adem�s descargo los uploaders. Descargo aqu� los uploaders por
		 * problemas de acceso desde AddRecurso (por ejemplo)
		 */

		@Override
		protected String doInBackground(String... params) {
			admin = params[0];
			if (admin.equals("si")) {
				if (isOnline()) {
					uploaders.descargarUploaders();
				} else {
					Toast.makeText(Asignaturas.this, "Error de conexi�n",
							Toast.LENGTH_SHORT).show();
				}
			}
			if (almacen.descargarAsig()) {
				return "ok";
			} else {
				return "error";
			}
		}

		/*
		 * Cerramos el ProgressDialog y ponemos el adaptador de asignaturas
		 */
		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.equals("ok")) {
				setListAdapter(new MiAdaptadorAsignaturas(Asignaturas.this,
						Almacen_asignaturas.getAlmacen()));
			}
		}

	}

}
