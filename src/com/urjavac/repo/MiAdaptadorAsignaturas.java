package com.urjavac.repo;

import java.util.Vector;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MiAdaptadorAsignaturas extends BaseAdapter {

	public Vector<Asignatura> almacen = new Vector<Asignatura>();
	public Activity actividad;
	
	
	public MiAdaptadorAsignaturas(Activity actividad, Vector<Asignatura> almacen) {
		super();
		this.almacen = almacen;
		this.actividad = actividad;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return almacen.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return almacen.elementAt(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater = actividad.getLayoutInflater();
		View view = inflater.inflate(R.layout.asignatura_elem, null, true);
		TextView id = (TextView)view.findViewById(R.id.id_addasig);
		TextView nombre = (TextView)view.findViewById(R.id.nombre_asig);
		TextView recursos = (TextView)view.findViewById(R.id.num_recursos);
		id.setText(almacen.elementAt(position).getId());
		nombre.setText(almacen.elementAt(position).getNombre());
		recursos.setText(String.valueOf(almacen.elementAt(position).getRecursos())+" recursos.");
		return view;
	}

	

}
