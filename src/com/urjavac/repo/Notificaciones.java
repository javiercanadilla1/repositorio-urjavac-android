package com.urjavac.repo;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;

import conexion_httpPostAux.HttpPostAux;

public class Notificaciones extends Activity implements Button.OnClickListener {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	String usuario;
	String notificacion;
	ProgressDialog pDialog;

	Button estado_notif;
//	Button compro_noti;
	TextView estado;

	String URL_connect = "http://www.urjavac.com/repo_android/repo.php";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notificaciones);
		Bundle bundle = getIntent().getExtras();
		usuario = bundle.getString("usuario");

		estado_notif = (Button) findViewById(R.id.act_desact_notif);
	//	compro_noti = (Button) findViewById(R.id.compro_noti);
		estado = (TextView) findViewById(R.id.recibe);

		preferences = getSharedPreferences("configuracion",
				Context.MODE_PRIVATE);
		if (usuario.equals(preferences.getString("usuario", ""))) {
			notificacion = preferences.getString("recibe_noti", "no");

			if (notificacion.equals("si")) {
				estado_notif.setText("Desactivar");
				estado.setText("Ahora mismo: recibe notificaciones.");
			} else {
				estado_notif.setText("Activar");
				estado.setText("Ahora mismo: no recibe notificaciones.");
			}
		} else {
			estado_notif.setText("Activar");
			estado.setText("Ahora mismo: no recibe notificaciones.");
		}

		estado_notif.setOnClickListener(this);
	//	compro_noti.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notificaciones, menu);
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == estado_notif.getId()) {
			editor = preferences.edit();
			if (estado_notif.getText().equals("Activar")) {
				estado_notif.setText("Desactivar");
				estado.setText("Ahora mismo: recibe notificaciones");

				editor.putString("usuario", usuario);
				editor.putString("recibe_noti", "si");
				editor.commit();
				GCMRegistrar.register(this, "786938045651");
			} else if (estado_notif.getText().equals("Desactivar")) {
				GCMRegistrar.unregister(this);
				estado_notif.setText("Activar");
				estado.setText("Ahora mismo: no recibe notificaciones");
				editor.putString("recibe_noti", "no");
				editor.commit();
			}
	//	} else if (v.getId() == compro_noti.getId()) {

		//	notificacionPrueba();

		}
	}

/*	public void notificacionPrueba() {
		new NotiPruebaTask().execute();
	}

	class NotiPruebaTask extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			HttpPostAux post = new HttpPostAux();

			ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();

			datos.add(new BasicNameValuePair("accion", "pruebaNoti"));
			datos.add(new BasicNameValuePair("usuario", usuario));
			Log.e("Notificacion Prueba", "datos enviados correctamente");
			JSONArray jdata = post.getserverdata(datos, URL_connect);

			return null;
		}

	} */
}
