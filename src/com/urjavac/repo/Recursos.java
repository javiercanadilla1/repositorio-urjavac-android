package com.urjavac.repo;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class Recursos extends ListActivity {
	private ProgressDialog pDialog;
	private String admin;
	private String asignatura;
	private String id_asig;
	private Almacen_recursos almacen = new Almacen_recursos();
	private int recursos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundle = getIntent().getExtras();
		admin = bundle.getString("admin");
		asignatura = bundle.getString("asignatura");
		id_asig = bundle.getString("id");
		recursos = bundle.getInt("recursos");
		
			setContentView(R.layout.activity_recursos);
			this.setTitle(asignatura);
			new AsyncRecursos().execute(id_asig, asignatura);
		

	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}

		return false;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Intent i = new Intent(Recursos.this, DetallesRecurso.class);
		i.putExtra("nombre", almacen.getAlmacen().elementAt(position)
				.getNombre());
		i.putExtra("descripcion", almacen.getAlmacen().elementAt(position)
				.getDescripcion());
		i.putExtra("enlace", almacen.getAlmacen().elementAt(position)
				.getEnlace());
		i.putExtra("formato", almacen.getAlmacen().elementAt(position)
				.getFormato());
		i.putExtra("curso", almacen.getAlmacen().elementAt(position).getCurso());
		i.putExtra("tipo", almacen.getAlmacen().elementAt(position).getTipo());
		i.putExtra("uploader", almacen.getAlmacen().elementAt(position)
				.getUploader());
		i.putExtra("oficial", almacen.getAlmacen().elementAt(position)
				.isOficial());
		startActivity(i);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.recursos, menu);
		return false;
	}
	
	/*
	 * S�lo lo utilizo para iniciar el Analytics.
	 * 
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}
	
	/*
	 * Igual para pararlo.
	 * 
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.

	}

	public class AsyncRecursos extends AsyncTask<String, String, String> {

		String id_asignatura;

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(Recursos.this);
			pDialog.setMessage("Descargando recursos...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			id_asignatura = params[0];
			asignatura = params[1];
			if (isOnline()) {
				if (almacen.descargarRecursos(id_asignatura, asignatura)) {
					return "ok";
				} else {
					return "error";
				}
			} else {
				Toast.makeText(Recursos.this, "Error de conexi�n",
						Toast.LENGTH_SHORT).show();
				return "error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.equals("ok")) {
				setListAdapter(new MiAdaptadorRecursos(Recursos.this,
						almacen.getAlmacen()));
			} else {
				Toast.makeText(Recursos.this, "Error", Toast.LENGTH_LONG)
						.show();
			}
		}

	}

}
