package com.urjavac.repo;

public class Uploader {
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Uploader(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	
}
