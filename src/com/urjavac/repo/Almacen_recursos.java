package com.urjavac.repo;

import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import conexion_httpPostAux.HttpPostAux;

public class Almacen_recursos {
	
	private Vector<Recurso> almacen = new Vector<Recurso>();
	private HttpPostAux post;
	private String URL_connect = "http://www.urjavac.com/repo_android/repo.php";


	public Vector<Recurso> getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Vector<Recurso> almacen) {
		this.almacen = almacen;
	}
	
	public boolean descargarRecursos(String id_asignatura, String asignatura){
		almacen=new Vector<Recurso>();
		post = new HttpPostAux();
		ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();

		datos.add(new BasicNameValuePair("accion", "getRecursos"));
		datos.add(new BasicNameValuePair("asignatura", id_asignatura));

		String nombre;
		
		JSONArray jdata = post.getserverdata(datos, URL_connect);

		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data;

			try {
				for (int i = 0; i < jdata.length(); i++) {
					json_data = jdata.getJSONObject(i);
					
					nombre = json_data.getString("nombre");
					String descripcion = json_data.getString("descripcion");
					String formato = json_data.getString("formato");
					int tipo = json_data.getInt("tipo");
					String curso = json_data.getString("curso");
					String enlace = json_data.getString("enlace");
					String ofi = json_data.getString("oficial");
					String uploader = json_data.getString("uploader");
					boolean oficial;
					if(ofi.equals("1")){
						oficial = true;
					}else
						oficial=false;
					Recurso r = new Recurso(nombre,descripcion,formato,tipo,curso,asignatura,enlace,oficial,uploader);
					
					almacen.add(r);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			return true;
			
		} else {
			System.out.print("Algo ha fallado...Linea 127");
			return false;
		}
	}

	
	
	
}
