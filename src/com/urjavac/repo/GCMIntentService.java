package com.urjavac.repo;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;

import conexion_httpPostAux.HttpPostAux;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCM Tutorial::Service";
	private static final String URL_connect = "http://www.urjavac.com/repo_android/repo.php";

	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	String usuario; 
	// Use your PROJECT ID from Google API into SENDER_ID
	public static final String SENDER_ID = "786938045651";

	public GCMIntentService() {
		super(SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "onRegistered: registrationId=" + registrationId);
		preferences = getSharedPreferences("configuracion", Context.MODE_PRIVATE);
		usuario = preferences.getString("usuario", "");
		new activarGCMTask().execute(usuario, registrationId);
	
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "onUnregistered: registrationId=" + registrationId);
		Toast.makeText(this, "Se ha dado de baja correctamente", Toast.LENGTH_LONG).show();
		preferences = getSharedPreferences("configuracion", Context.MODE_PRIVATE);
		usuario = preferences.getString("usuario", "");
		new desactivarGCMTask().execute(usuario);
	}


	@Override
	protected void onMessage(Context context, Intent data) {
		String message;
		String nombre;
		String descripcion;
		String ofi;
		boolean oficial;
		String enlace;
		String uploader;
		String curso;
		String tipo;
		String formato;
		
		// Message from PHP server
		message = data.getStringExtra("message");
		nombre = data.getStringExtra("nombre");
		descripcion = data.getStringExtra("descripcion");
		ofi = data.getStringExtra("oficial");
		oficial = (ofi.equals("si"))?true:false;
		enlace = data.getStringExtra("enlace");
		uploader = data.getStringExtra("uploader");
		curso = data.getStringExtra("curso");
		tipo = data.getStringExtra("tipo");
		formato = data.getStringExtra("formato");
		// Open a new activity called MainActivity
		Intent intent = new Intent(this, DetallesRecurso.class);
		
		intent.putExtra("nombre", nombre);
		intent.putExtra("descripcion", descripcion);
		intent.putExtra("enlace", enlace);
		intent.putExtra("formato", formato);
		intent.putExtra("curso", curso);
		intent.putExtra("tipo", tipo);
		intent.putExtra("uploader", uploader);
		intent.putExtra("oficial", oficial);
		
		// Starts the activity on notification click
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		// Create the notification with a notification builder
		Notification notification = new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.icono_app)
				.setWhen(System.currentTimeMillis())
				.setContentTitle("Repositorio URJavaC")
				.setContentText(message).setContentIntent(pIntent).setDefaults(Notification.DEFAULT_ALL)
				.build();
		// Remove the notification on click
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		manager.notify(R.string.app_name, notification);

		{
			// Wake Android Device when notification received
			PowerManager pm = (PowerManager) context
					.getSystemService(Context.POWER_SERVICE);
			final PowerManager.WakeLock mWakelock = pm.newWakeLock(
					PowerManager.PARTIAL_WAKE_LOCK
							| PowerManager.ACQUIRE_CAUSES_WAKEUP, "GCM_PUSH");
			mWakelock.acquire();

			// Timer before putting Android Device to sleep mode.
			Timer timer = new Timer();
			TimerTask task = new TimerTask() {
				public void run() {
					mWakelock.release();
				}
			};
			timer.schedule(task, 5000);
		}

	}

	@Override
	protected void onError(Context arg0, String errorId) {

		Log.e(TAG, "onError: errorId=" + errorId);
	}

	class activarGCMTask extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... arg0) {
			String usuario = arg0[0];
			String result ="";
			String regId = arg0[1];
			
			HttpPostAux post = new HttpPostAux();

			ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();

			datos.add(new BasicNameValuePair("accion", "regGCM"));
			datos.add(new BasicNameValuePair("usuario", usuario));
			datos.add(new BasicNameValuePair("gcm", regId));

			JSONArray jdata = post.getserverdata(datos, URL_connect);

			if (jdata != null && jdata.length() > 0) {

				JSONObject json_data;

				try {
					json_data = jdata.getJSONObject(0);
					result = json_data.getString("result");
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (result.equals("si")) {
					return "ok";
				} else {
					return "error";
				}
			}

			return "error";
		}
	}
	
class desactivarGCMTask extends AsyncTask<String, String, String> {
		
		@Override
		protected String doInBackground(String... arg0) {
			String usuario = arg0[0];
			String result = "";
			
			HttpPostAux post = new HttpPostAux();

			ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();

			datos.add(new BasicNameValuePair("accion", "quitarGCM"));
			datos.add(new BasicNameValuePair("usuario", usuario));

			JSONArray jdata = post.getserverdata(datos, URL_connect);

			if (jdata != null && jdata.length() > 0) {

				JSONObject json_data;

				try {
					json_data = jdata.getJSONObject(0);
					result = json_data.getString("quitar");
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (result.equals("si")) {
					return "ok";
				} else {
					return "error";
				}
			}

			return "error";
		}

	}
	
}