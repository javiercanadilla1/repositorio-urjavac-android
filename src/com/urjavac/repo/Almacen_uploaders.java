package com.urjavac.repo;

import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import conexion_httpPostAux.HttpPostAux;

public class Almacen_uploaders {
	
	private static Vector<Uploader> almacen_uploaders = new Vector<Uploader>();
	private String URL_connect = "http://www.urjavac.com/repo_android/repo.php";
	private HttpPostAux post = new HttpPostAux();
	
	public static Vector<Uploader> getAlmacen_uploaders() {
		return almacen_uploaders;
	}
	public void setAlmacen_uploaders(Vector<Uploader> almacen_uploaders) {
		Almacen_uploaders.almacen_uploaders = almacen_uploaders;
	}
	
	public boolean descargarUploaders(){
		post = new HttpPostAux();
		ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();

		datos.add(new BasicNameValuePair("accion", "uploaders"));

		String nombre;

		JSONArray jdata = post.getserverdata(datos, URL_connect);

		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data;

			try {
				for (int i = 0; i < jdata.length(); i++) {
					json_data = jdata.getJSONObject(i);
					nombre = json_data.getString("nombre");
					Uploader a = new Uploader(nombre);
					almacen_uploaders.add(a);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return true;
			
		} else {
			System.out.print("Algo ha fallado...Linea 127");
			return false;
		}
	
	}
	
}
