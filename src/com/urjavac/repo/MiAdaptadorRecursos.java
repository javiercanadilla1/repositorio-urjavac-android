package com.urjavac.repo;

import java.util.Vector;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MiAdaptadorRecursos extends BaseAdapter {

	private Vector<Recurso> almacen = new Vector<Recurso>();
	private Activity actividad;

	public MiAdaptadorRecursos(Activity actividad, Vector<Recurso> almacen) {
		super();
		this.almacen = almacen;
		this.actividad = actividad;
	}

	public Vector<Recurso> getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Vector<Recurso> almacen) {
		this.almacen = almacen;
	}

	public Activity getActividad() {
		return actividad;
	}

	public void setActividad(Activity actividad) {
		this.actividad = actividad;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return almacen.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return almacen.elementAt(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String strTipo;
		int tipo;
		LayoutInflater inflater = actividad.getLayoutInflater();
		View view = inflater.inflate(R.layout.recurso_elem, null, true);
		ImageView bTipo = (ImageView) view.findViewById(R.id.img_tipo);
		tipo = almacen.elementAt(position).getTipo();
		TextView nombre = (TextView) view.findViewById(R.id.titulo_recurso);
		TextView descripcion = (TextView) view
				.findViewById(R.id.descripcion_rec);
		TextView curso = (TextView) view.findViewById(R.id.curso_rec);
		if (almacen.elementAt(position).getTipo() == 1) {
			bTipo.setImageResource(R.drawable.icon_apuntes);
		} else if (almacen.elementAt(position).getTipo() == 2
				|| almacen.elementAt(position).getTipo() == 3) {
			bTipo.setImageResource(R.drawable.icon_examen);
		} else if (almacen.elementAt(position).getTipo() == 4
				|| almacen.elementAt(position).getTipo() == 5) {
			bTipo.setImageResource(R.drawable.icon_practica);
		}
		nombre.setText(almacen.elementAt(position).getNombre());
		if (almacen.elementAt(position).getDescripcion().equals("")) {
			descripcion.setText("No hay ninguna descripción adicional.");
		} else {
			descripcion.setText(almacen.elementAt(position).getDescripcion());
		}
		curso.setText(almacen.elementAt(position).getCurso());
		return view;
	}

}
