package com.urjavac.repo;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class EnviarRecurso extends Activity implements Button.OnClickListener {

	int tipo;
	String usuario;
	private CheckBox oficial;
	private EditText nombre;
	private EditText descripcion;
	private EditText enlace;
	private Button enviar;
	private Spinner curso;
	private Spinner spinAsignatura;
	private Spinner formato;
	private Vector<Asignatura> almacen;
	private String[] asignaturas;
	private String oficialInt;
	private String recurso;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enviar_recurso);
		Bundle bundle = getIntent().getExtras();
		tipo = bundle.getInt("tipo");
		usuario = bundle.getString("usuario");

		/*
		 * Castings
		 */
		oficial = (CheckBox) findViewById(R.id.oficial_envio);
		nombre = (EditText) findViewById(R.id.nombre_recurso_envio);
		descripcion = (EditText) findViewById(R.id.descripcion_envio);
		enlace = (EditText) findViewById(R.id.enlace_envio);
		enviar = (Button) findViewById(R.id.enviar_recurso_mail);
		enviar.setOnClickListener(this);
		curso = (Spinner) findViewById(R.id.spin_curso_envio);
		spinAsignatura = (Spinner) findViewById(R.id.spin_asig_envio);
		formato = (Spinner) findViewById(R.id.formato_envio);

		/*
		 * Adaptar las asignaturas a un Array para ponerlo en el Spinner
		 */
		almacen = Almacen_asignaturas.getAlmacen();
		asignaturas = new String[almacen.size()];
		for (int i = 0; i < almacen.size(); i++) {
			asignaturas[i] = almacen.elementAt(i).getNombre();
		}
		ArrayAdapter<String> adaptaAsig = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, asignaturas);
		spinAsignatura.setAdapter(adaptaAsig);

		/*
		 * Modificar el Activity conforme al bot�n pulsado.
		 */
		switch (tipo) {
		case 1:
			this.setTitle("Enviar Apuntes");
			recurso = "unos apuntes";
			break;
		case 2:
			this.setTitle("Enviar Examen");
			oficial.setVisibility(View.INVISIBLE);
			recurso = "un examen";
			break;
		case 3:
			this.setTitle("Enviar Sol. Examen");
			recurso = "una soluci�n de examen";
			break;
		case 4:
			this.setTitle("Enviar Pr�ctica");
			oficial.setVisibility(View.INVISIBLE);
			recurso = "una pr�ctica";
			break;
		case 5:
			this.setTitle("Enviar Sol.Pr�ctica");
			recurso = "una soluci�n de pr�ctica";
			break;
		}
		oficialInt = "0";
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.enviar_recurso, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == enviar.getId()) {
			Calendar d = Calendar.getInstance();
			if (oficial.isChecked()) {
				oficialInt = "S�";
			} else {
				oficialInt = "No";
			}
			if (oficial.getVisibility() == View.INVISIBLE) {
				oficialInt = "S�";
			}
			/*
			 * Crear el intent para enviar el email.
			 */
			Intent mail = new Intent(Intent.ACTION_SEND);
			mail.setType("text/plain");
			mail.putExtra(Intent.EXTRA_EMAIL,
					new String[] { "repo@urjavac.com" });
			mail.putExtra(Intent.EXTRA_SUBJECT, "Envio de " + recurso
					+ " desde la app del Repositorio.");
			mail.putExtra(
					Intent.EXTRA_TEXT,
					"El usuario: "
							+ usuario
							+ " ha enviado el siguiente recurso:\n\tNombre: "
							+ nombre.getText().toString()
							+ "\n\tDecripci�n: "
							+ descripcion.getText().toString()
							+ "\n\tEnlace: "
							+ enlace.getText().toString()
							+ "\n\tAsignatura: "
							+ spinAsignatura.getSelectedItem().toString()
							+ "\n\tCurso: "
							+ curso.getSelectedItem().toString()
							+ "\n\tOficial: "
							+ oficialInt
							+ "\n\tY con Formato: "
							+ formato.getSelectedItem().toString()
							+ ".\nSe ha enviado a las: "
							+ d.getTime().toString()
							+ ".\n\nPor favor, no modifique el contenido de �ste mensaje. Est� configurado para que se env�e a la direcci�n correcta."
							+ "\nGracias por su atenci�n y por utilizar �sta aplicaci�n.");
			startActivity(mail);
			finish();
			Toast.makeText(
					this,
					"Elija la aplicaci�n con la que realizar el env�o del recurso.",
					Toast.LENGTH_LONG).show();
		}
	}

	/*
	 * S�lo lo utilizo para iniciar el Analytics.
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	/*
	 * Igual para pararlo.
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

}
