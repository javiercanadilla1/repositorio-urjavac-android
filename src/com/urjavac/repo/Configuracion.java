package com.urjavac.repo;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Configuracion extends ListActivity {
	
	String entradas[] = {"Notificaciones", "Repositorio URJavaC - Android"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion);
		setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,entradas));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.configuracion, menu);
		return false;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if(v.getId()==0){
			Intent i = new Intent (Configuracion.this, Notificaciones.class);
			startActivity(i);
		}
	}
	
}
