package com.urjavac.repo;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

import conexion_httpPostAux.HttpPostAux;

public class MainActivity extends Activity implements Button.OnClickListener {

	private SharedPreferences preferencias;
	private SharedPreferences.Editor editor;

	private ProgressDialog pDialog;

	private EditText user;
	private EditText pass;
	private Button bLogin;

	private CheckBox recordarme;

	private String nombre;
	private String contrase�a;

	private HttpPostAux post;

	protected boolean errorLogin = false;

	private String URL_connect = "http://www.urjavac.com/repo_android/acceso.php";

	/*M�todo OnCreate:
	 * Recupero el nombre de usuario y la contrase�a si los hay, sino asigno a la variable nombre y contrase�a un ""
	 * Despu�s compruebo que no son vac�as para lanzar el login sin tener que dar al boton de Login.
	 * Por �ltimo, hago los casting para la interfaz.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		preferencias = getSharedPreferences("Login", Context.MODE_PRIVATE);
		nombre = preferencias.getString("Nombre", "");
		contrase�a = preferencias.getString("Password", "");
		setContentView(R.layout.activity_main);

		if (!nombre.equals("") && !contrase�a.equals("")) {
			new AsyncLogin().execute(nombre, contrase�a);
		}
		user = (EditText) findViewById(R.id.user);
		pass = (EditText) findViewById(R.id.pass);

		bLogin = (Button) findViewById(R.id.bLogin);
		bLogin.setOnClickListener(this);
		recordarme = (CheckBox) findViewById(R.id.recordarme);
	}

	/*M�todo OnClick:
	 * if (v.getId() == bLogin.getId()) {
	 * Si se ha producido un error durante la sesi�n, pongo el nombre y el password a "" para no entrar en un bucle infinito.
	 * Y le asigno el valor que el usuario ha introducido desde la interfaz.
	 * 
	 * Si el checkbox est� activado guardo el nombre de usuario y la pass en las preferencias.
	 * 
	 * Compruebo si el dispositivo tiene conexion a internet isOnline(). Si tiene ejecuto el Login.
	 * 
	 */
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == bLogin.getId()) {
			editor = preferencias.edit();
			if (errorLogin) {
				editor.putString("Nombre", "");
				editor.putString("Password", "");
				editor.commit();
				nombre = user.getText().toString();
				contrase�a = pass.getText().toString();
			}
			if (nombre.equals("") && contrase�a.equals("")) {
				nombre = user.getText().toString();
				contrase�a = pass.getText().toString();
			}
			if (recordarme.isChecked()) {

				editor.putString("Nombre", nombre);
				editor.putString("Password", contrase�a);
				editor.commit();
			}
			if (isOnline()) {
				new AsyncLogin().execute(nombre, contrase�a);
			} else {
				Toast.makeText(this, "Error. Es necesario conexi�n a Internet",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	
	//Comprueba si tiene conexion a internet.
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}

		return false;
	}
	//Se podr�a eliminar, si esta a false el men� no se muestra. Si lo ponemos a True si que lo mostrar�a con el contenido que hay en R.menu.main.
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}
	/*
	 * S�lo lo utilizo para iniciar el Analytics.
	 * 
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}
	
	/*
	 * Igual para pararlo.
	 * 
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.

	}
	
	/*M�todo Login: Devuelve un Array de booleanos -> posicion 1: Login correcto/incorrecto posicion 2:Admin/no Admin.
	 * 
	 * 
	 */
	public boolean[] login(String u, String c) { // POS 0: LOGIN POS 1: ADMIN?

		int login = -1;
		int a = 0;
		boolean[] sol = { false, false };

		post = new HttpPostAux();
		ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();

		datos.add(new BasicNameValuePair("usuario", u.toString()));		//Paso los parametros Usuario y Password por: $_POST en php con esas etiquetas.
		datos.add(new BasicNameValuePair("password", c.toString()));	//"usuario" y "password"

		JSONArray jdata = post.getserverdata(datos, URL_connect);	//Realizo la conexi�n al servidor.

		if (jdata != null && jdata.length() > 0) {	//Compruebo la respuesta del servidor.

			JSONObject json_data;

			try {
				json_data = jdata.getJSONObject(0);	//Recojo la posicion  (s�lo hay esa)
				login = json_data.getInt("login");	//Entro en el campo "login" y saco su valor.

				json_data = jdata.getJSONObject(0);
				a = json_data.getInt("admin");	//Igual para el campo admin.

			} catch (JSONException e) {
				e.printStackTrace();
			}
			//Asigno lo que corresponda en cada caso. Es importante lo que devuelve el servidor.
			if (a == 1) {	
				sol[1] = true;	//Es admin
			} else {
				sol[1] = false;
			}

			if (login == 1) { // Todo correcto
				sol[0] = true;
			} else {
				sol[0] = false;
			}

			return sol;
		} else {
			System.out.print("Algo ha fallado...Linea 112");
			return sol;
		}

	}
	/*Clase necesaria para poder realizar la consulta al servidor y poder hacer el Login.
	 * 
	 * 
	 */
	class AsyncLogin extends AsyncTask<String, String, String> {

		private String usuario, password;

		@Override
		protected void onPreExecute() {
			// Se inicializa y lanza el ProgressDialog. Superfacil ;)
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Identificando...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// Se obtienen los par�metros que se le pasa en la linea 65 o 109 ---.execute(usuario,contrase�a);
			usuario = params[0];
			password = params[1];
			boolean[] login = login(usuario, password);
			if (login[0] && login[1]) { // LOGIN OK ADMIN OK
				return "admin";
			} else if (login[0] && (login[1] == false)) { // LOGIN OK, NO ADMIN
				return "user";
			} else {
				return "error";
			}

		}
		/*Si todo a salido bien se inicia el Activity: Asignaturas.class
		 * 
		 * 
		 */
		@Override
		protected void onPostExecute(String result) {
			// Se detiene el ProgressDialog. Importante.
			pDialog.dismiss();
			if (result.equals("admin")) {	//Comprobamos si es Admin y le pasamos los par�metros correspondientes para tener acceso al menu Admin
				Intent i = new Intent(MainActivity.this, Asignaturas.class);
				i.putExtra("usuario", usuario.toString());
				i.putExtra("admin", "si");
				i.putExtra("inicio", true);
				finish();
				startActivity(i);
			} else if (result.equals("user")) {
				Intent i = new Intent(MainActivity.this, Asignaturas.class);
				i.putExtra("usuario", usuario.toString());
				i.putExtra("admin", "no");
				i.putExtra("inicio", true);
				finish();
				startActivity(i);
			} else if (result.equals("error")) {	//No har�a falta comprobar si es .equals("error") con un else valdr�a.
				Toast.makeText(MainActivity.this,
						"Usuario o contrase�a incorrectos", Toast.LENGTH_LONG)
						.show();
				errorLogin = true;
			}
		}

	}

}

/*
 *	�ltima actualizaci�n:
 *		He a�adido los comentarios y el c�digo necesario para el Google Analytics. 
 * 
*/