package com.urjavac.repo;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import conexion_httpPostAux.HttpPostAux;

public class AddAsignatura extends Activity implements Button.OnClickListener{

	EditText ID;
	EditText nombre;
	Button enviar;
	
	ProgressDialog pDialog;
	HttpPostAux post;
	
	private String URL_connect = "http://www.urjavac.com/repo_android/repo.php";
	
	/*
	 * M�todo OnCreate normal:
	 * Castings y cambio de titulo del activity.
	 * 
	 */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_asignatura);
		this.setTitle("A�adir Asignatura");
		ID = (EditText)findViewById(R.id.id_addasig);
		nombre = (EditText)findViewById(R.id.nombre_addasig);
		enviar = (Button)findViewById(R.id.b_addAsignatura);
		enviar.setOnClickListener(this);
	}
	
	/*
	 * Selecci�n de XML men�
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_asignatura, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==enviar.getId()){
			
		}
	}

	public boolean crearAsignatura(String id, String nombre){
		int result;
		post=new HttpPostAux();
		ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();
		
		datos.add(new BasicNameValuePair("accion", "addAsignatura"));

		
		datos.add(new BasicNameValuePair("abreviatura", id));
		datos.add(new BasicNameValuePair("nombre", nombre));


		JSONArray jdata = post.getserverdata(datos, URL_connect);

		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data;
			
			try{
				json_data = jdata.getJSONObject(0);
				result = json_data.getInt("asignatura");
				
				if(result == 1){
					return true;
				}else {
					return false;
				}
				
			}catch(JSONException e){
				e.printStackTrace();
				return false;
			}
		}else{
			Toast.makeText(this, "Error en linea 175", Toast.LENGTH_LONG).show();
			return false;
		}
	
	}
	
	class asyncAddAsignatura extends AsyncTask<String, String, String>{

		String id, nombre;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pDialog = new ProgressDialog(AddAsignatura.this);
			pDialog.setMessage("Agregando asignatura...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			id = params[0];
			nombre = params[1];
			if(crearAsignatura(id,nombre)){
				
				return "ok";
			}else{
				return "error";
			}
			
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			pDialog.dismiss();
			if(result.equals("ok")){
				Toast.makeText(AddAsignatura.this, "Asignatura agregada correctamente", Toast.LENGTH_SHORT).show();
				finish();
			}else{
				Toast.makeText(AddAsignatura.this, "Ha ocurrido un error al agregar la asignatura.", Toast.LENGTH_SHORT).show();
			}
			
		}
	}
	
}
