package com.urjavac.repo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Environment;
import android.util.Log;
import conexion_httpPostAux.HttpPostAux;

public class Almacen_asignaturas {

	private static Vector<Asignatura> almacen = new Vector<Asignatura>();

	private String URL_connect = "http://www.urjavac.com/repo_android/repo.php";

	private HttpPostAux post = new HttpPostAux();

	public static Vector<Asignatura> getAlmacen() {
		return almacen;
	}

	public static void guardarAlmacen() {
		String estado = Environment.getExternalStorageState();
		File directorio;
		if (estado.equals(Environment.MEDIA_MOUNTED)) {
			directorio = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/Repositorio_URJavaC");
		} else {
			directorio = new File(Environment.getDataDirectory()
					.getAbsolutePath());
		}
		if (!directorio.exists()) {
			directorio.mkdir();
		}
		try {
			File f = new File(directorio, "asignaturas.txt");
			f.createNewFile();
			PrintWriter salida = new PrintWriter(f);
			for (int i = 0; i < almacen.size(); i++) {
				salida.println("Asignatura");
				salida.println(almacen.get(i).getId());
				salida.println(almacen.get(i).getNombre());
				salida.println(String.valueOf(almacen.get(i).getRecursos()));
				salida.println();
			}
			salida.close();
		} catch (Exception e) {
			Log.e("Error de escritura", e.getMessage(), e);
		}
	}

	public static void leerAlmacen() {
		File directorio = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/Repositorio_URJavaC");
		almacen = new Vector<Asignatura>();
		try {
			File f = new File(directorio, "asignaturas.txt");
			FileInputStream file = new FileInputStream(f);
			BufferedReader entrada = new BufferedReader(new InputStreamReader(
					file));
			String linea;
			Asignatura asignatura;
			linea = entrada.readLine();
			do {
				if (linea.contains("Asignatura")) {
					linea = entrada.readLine();
					String id = linea.toString();
					linea = entrada.readLine();
					String nombre = linea.toString();
					linea = entrada.readLine();
					int recursos = Integer.parseInt(linea.toString());
					asignatura = new Asignatura(id, nombre, recursos);
					almacen.add(asignatura);
					linea = entrada.readLine();
				}
				linea = entrada.readLine();
			} while (linea != null);
			entrada.close();
		} catch (Exception e) {
			Log.e("Error de Lectura", e.getMessage(), e);
		}
	}

	public boolean descargarAsig() {

		post = new HttpPostAux();
		ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();

		datos.add(new BasicNameValuePair("accion", "asignaturas"));

		String nombre;
		String id;
		int recursos;

		JSONArray jdata = post.getserverdata(datos, URL_connect);

		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data;

			try {
				for (int i = 0; i < jdata.length(); i++) {
					json_data = jdata.getJSONObject(i);
					id = json_data.getString("id");
					nombre = json_data.getString("nombre");
					recursos = json_data.getInt("recursos");
					Asignatura a = new Asignatura (id, nombre,recursos);
					almacen.add(a);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			guardarAlmacen();
			return true;
			
		} else {
			System.out.print("Algo ha fallado...Linea 127");
			return false;
		}
	}

}
