package com.urjavac.repo;

import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import conexion_httpPostAux.HttpPostAux;

public class AddRecurso extends Activity implements Button.OnClickListener {

	private Spinner formato;
	private int tipo;
	private Vector<Asignatura> almacen;
	private String[] asignaturas;
	private Spinner spinAsignatura;
	private Spinner upload;
	private Spinner curso;
	private String[] uploader;
	private Button enviar;
	private EditText nombre;
	private EditText descripcion;
	private EditText enlace;
	private CheckBox oficial;
	private String asignatura;
	private String oficialInt;
	private HttpPostAux post;
	
	private ProgressDialog pDialog;
	
	private String URL_connect = "http://www.urjavac.com/repo_android/repo.php";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_add_recurso);

		upload = (Spinner) findViewById(R.id.spin_uploader);

		uploader = new String[Almacen_uploaders.getAlmacen_uploaders().size()];

		for (int i = 0; i < Almacen_uploaders.getAlmacen_uploaders().size(); i++) {
			uploader[i] = Almacen_uploaders.getAlmacen_uploaders().elementAt(i)
					.getNombre();
		}
		ArrayAdapter<String> adaptaUpload = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, uploader);
		upload.setAdapter(adaptaUpload);

		almacen = Almacen_asignaturas.getAlmacen();
		asignaturas = new String[almacen.size()];
		for (int i = 0; i < almacen.size(); i++) {
			asignaturas[i] = almacen.elementAt(i).getNombre();
		}
		
		formato=(Spinner)findViewById(R.id.formato);
		
		ArrayAdapter<String> adaptaAsig = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, asignaturas);
		spinAsignatura = (Spinner) findViewById(R.id.spin_asig);
		spinAsignatura.setAdapter(adaptaAsig);

		oficial = (CheckBox) findViewById(R.id.oficial);
		Bundle bundle = getIntent().getExtras();
		tipo = bundle.getInt("tipo");
		switch (tipo) {
		case 1:
			this.setTitle("A�adir Apuntes");
			break;
		case 2:
			this.setTitle("A�adir Examen");
			oficial.setVisibility(View.INVISIBLE);
			break;
		case 3:
			this.setTitle("A�adir Sol. Examen");
			break;
		case 4:
			this.setTitle("A�adir Pr�ctica");
			oficial.setVisibility(View.INVISIBLE);
			break;
		case 5:
			this.setTitle("A�adir Sol.Pr�ctica");
			break;
		}

		nombre = (EditText) findViewById(R.id.nombre_recurso);
		descripcion = (EditText) findViewById(R.id.descripcion);
		enlace = (EditText) findViewById(R.id.enlace);
		enviar = (Button) findViewById(R.id.enviar);
		enviar.setOnClickListener(this);
		curso = (Spinner) findViewById(R.id.spin_curso);
		
		oficialInt = "0";
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_recurso, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == enviar.getId()) {
			if(oficial.isChecked()){
				oficialInt = "1";
			}else{
				oficialInt = "0";
			}
			if(oficial.getVisibility() == View.INVISIBLE){
				oficialInt = "1";
			}
			asignatura = spinAsignatura.getSelectedItem().toString();
			new AsyncRecurso().execute(asignatura, nombre.getText().toString(),
					descripcion.getText().toString(),
					formato.getSelectedItem().toString(),
					curso.getSelectedItem().toString(),
					enlace.getText().toString(), String.valueOf(tipo), oficialInt, upload
							.getSelectedItem().toString());
		}

	}

	public boolean enviarRecurso(String asignatura,String nombre,String descripcion,String formato,String curso, String enlace,String tipo,String oficial,String uploader) {
		int result;
		post = new HttpPostAux();
		ArrayList<NameValuePair> datos = new ArrayList<NameValuePair>();
		
		datos.add(new BasicNameValuePair("accion", "addRecurso"));

		datos.add(new BasicNameValuePair("asignatura", asignatura));
		datos.add(new BasicNameValuePair("nombre", nombre));
		datos.add(new BasicNameValuePair("descripcion", descripcion));
		
		/*<item>PDF</item>
        <item>ZIP/RAR</item>
        <item>ODT</item>
        <item>DOC/DOCX</item>
        <item>Imagen</item>
        <item>JAVA</item>
        <item>PAS</item>
        <item>Otro</item>
        
        ----
        <option value="pdf">PDF
		<option value="comp">ZIP/RAR
		<option value="odt">ODT
		<option value="doc">DOC/DOCX
		<option value="img">Imagen
		<option value="java">JAVA
		<option value="pascal">PAS
		<option value="otro">Otro
        */
		
		if(formato.equals("PDF")){
			datos.add(new BasicNameValuePair("formato", "pdf"));
		}else if(formato.equals("ZIP/RAR")){
			datos.add(new BasicNameValuePair("formato", "comp"));
		}else if(formato.equals("ODT")){
			datos.add(new BasicNameValuePair("formato", "odt"));
		}else if(formato.equals("DOC/DOCX")){
			datos.add(new BasicNameValuePair("formato", "doc"));
		}else if(formato.equals("Imagen")){
			datos.add(new BasicNameValuePair("formato", "img"));
		}else if(formato.equals("JAVA")){
			datos.add(new BasicNameValuePair("formato", "java"));
		}else if(formato.equals("PAS")){
			datos.add(new BasicNameValuePair("formato", "pascal"));
		}else if(formato.equals("Otro")){
			datos.add(new BasicNameValuePair("formato", "otro"));
		}
			
		datos.add(new BasicNameValuePair("curso", curso));
		datos.add(new BasicNameValuePair("enlace", enlace));
		datos.add(new BasicNameValuePair("tipo", tipo));
		datos.add(new BasicNameValuePair("oficial", oficial));
		datos.add(new BasicNameValuePair("uploader", uploader));

		
		
		JSONArray jdata = post.getserverdata(datos, URL_connect);

		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data;
			
			try{
				json_data = jdata.getJSONObject(0);
				result = json_data.getInt("recurso");
				
				if(result == 1){
					return true;
				}else {
					return false;
				}
				
			}catch(JSONException e){
				e.printStackTrace();
				return false;
			}
		}else{
			Toast.makeText(this, "Error en linea 175", Toast.LENGTH_LONG).show();
			return false;
		}
		
	}

	class AsyncRecurso extends AsyncTask<String, String, String> {
		private String asignatura, nombre, descripcion, formato, curso, enlace, tipo, oficial, uploader;
		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(AddRecurso.this);
			pDialog.setMessage("A�adiendo recurso...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {	
			asignatura = params[0];
			nombre = params[1];
			descripcion = params[2];
			formato = params[3];
			curso = params[4];
			enlace = params[5];
			tipo = params[6];
			oficial = params[7];
			uploader = params[8];
			if(enviarRecurso(asignatura, nombre, descripcion, formato, curso, enlace, tipo, oficial, uploader)){
				return "ok";
			}else{
				return "error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.equals("ok")) {
				Toast.makeText(AddRecurso.this,
						"Recurso a�adido correctamente.", Toast.LENGTH_SHORT)
						.show();
				finish();
			}else{
				Toast.makeText(AddRecurso.this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
			}
		}
	}

}
