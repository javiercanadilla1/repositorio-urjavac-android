package com.urjavac.repo;

public class Recurso {
	String nombre;
	String descripcion;
	String formato;
	int Tipo;
	String asignatura;
	String curso;
	String enlace;
	boolean oficial;
	String uploader;
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public int getTipo() {
		return Tipo;
	}
	public void setTipo(int tipo) {
		Tipo = tipo;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getEnlace() {
		return enlace;
	}
	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}
	public boolean isOficial() {
		return oficial;
	}
	public void setOficial(boolean oficial) {
		this.oficial = oficial;
	}
	public String getUploader() {
		return uploader;
	}
	public void setUploader(String uploader) {
		this.uploader = uploader;
	}
	
	public Recurso(String nombre, String descripcion, String formato, int tipo,
			String curso, String asignatura, String enlace, boolean oficial, String uploader) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.formato = formato;
		Tipo = tipo;
		this.curso = curso;
		this.asignatura = asignatura;
		this.enlace = enlace;
		this.oficial = oficial;
		this.uploader = uploader;
	}
	
	
}
