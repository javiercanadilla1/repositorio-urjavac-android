package com.urjavac.repo;

public class Asignatura {
	
	private String id;
	private String nombre;
	private int recursos;
	
	
	public Asignatura(String id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	public Asignatura(String id, String nombre, int recursos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.recursos = recursos;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getRecursos() {
		return recursos;
	}
	public void setRecursos(int recursos) {
		this.recursos = recursos;
	}
	
	
}
