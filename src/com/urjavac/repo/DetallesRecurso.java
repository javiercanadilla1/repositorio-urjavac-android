package com.urjavac.repo;

import com.google.analytics.tracking.android.EasyTracker;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetallesRecurso extends Activity implements OnClickListener {

	private String nombre;
	private String descripcion;
	private String enlace;
	private String formato;
	private String curso;
	private String uploader;
	private int tipo;
	String strFormato;
	private boolean oficial;

	private TextView nombre_recurso;
	private TextView descrip_recurso;
	private EditText enlace_recurso;
	private TextView curso_recurso;
	private TextView uploader_recurso;
	private ImageButton img_oficial;
	private ImageButton img_formato;
	private Button b_copiar;
	private Button b_descargar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_detalles_recurso);
		this.setTitle("Detalles del recurso");
		Bundle bundle = getIntent().getExtras();
		nombre = bundle.getString("nombre");
		descripcion = bundle.getString("descripcion");
		enlace = bundle.getString("enlace");
		formato = bundle.getString("formato");
		curso = bundle.getString("curso");
		tipo = bundle.getInt("tipo");
		uploader = bundle.getString("uploader");
		oficial = bundle.getBoolean("oficial");

		nombre_recurso = (TextView) findViewById(R.id.det_nombre_rec);
		descrip_recurso = (TextView) findViewById(R.id.det_desc_rec);
		enlace_recurso = (EditText) findViewById(R.id.det_enlace);
		curso_recurso = (TextView) findViewById(R.id.det_curso_rec);
		uploader_recurso = (TextView) findViewById(R.id.det_uploader);
		img_oficial = (ImageButton) findViewById(R.id.img_oficial);
		img_formato = (ImageButton) findViewById(R.id.img_tipo);

		img_oficial.setOnClickListener(this);
		img_formato.setOnClickListener(this);
		
		nombre_recurso.setText(nombre);
		if (descripcion.equals("")) {
			descrip_recurso.setText("No hay ninguna descripci�n adicional.");
		} else {
			descrip_recurso.setText(descripcion);
		}
		enlace_recurso.setText(enlace);
		curso_recurso.setText(curso);
		uploader_recurso.setText(uploader);

		if (oficial) {
			img_oficial.setImageResource(R.drawable.boton_of_verde);
		} else {
			img_oficial.setImageResource(R.drawable.boton_of_azul);
		}

		/*
		 * <option value="pdf">PDF <option value="comp">ZIP/RAR <option
		 * value="odt">ODT <option value="doc">DOC/DOCX <option
		 * value="img">Imagen <option value="java">JAVA <option
		 * value="pascal">PAS <option value="otro">Otro
		 */
		if (formato.equals("pdf")) {
			strFormato="PDF";
			img_formato.setImageResource(R.drawable.boton_tipo_pdf);
		} else if (formato.equals("comp")) {
			strFormato="ZIP/RAR";
			img_formato.setImageResource(R.drawable.boton_tipo_rar);
		} else if (formato.equals("odt")) {
			strFormato="ODT";
			img_formato.setImageResource(R.drawable.boton_tipo_odt);
		} else if (formato.equals("doc")) {
			strFormato="DOC/DOCX";
			img_formato.setImageResource(R.drawable.boton_tipo_doc);
		} else if (formato.equals("img")) {
			strFormato="IMG";
			img_formato.setImageResource(R.drawable.boton_tipo_img);
		} else if (formato.equals("java")) {
			strFormato="C�digo Java";
			img_formato.setImageResource(R.drawable.boton_tipo_java);
		} else if (formato.equals("pascal")) {
			strFormato="C�digo Pascal";
			img_formato.setImageResource(R.drawable.boton_tipo_pas);
		} else if (formato.equals("otro")) {
			strFormato="Otro";
			img_formato.setImageResource(R.drawable.boton_tipo_file);
		}

		b_copiar = (Button)findViewById(R.id.det_copiar);
		b_descargar = (Button)findViewById(R.id.det_descargar);
		
		b_copiar.setOnClickListener(this);
		b_descargar.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detalles_recurso, menu);
		return false;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.det_copiar){
		    ClipboardManager ClipMan = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		    ClipData clip = ClipData.newPlainText("url", enlace);
		    ClipMan.setPrimaryClip(clip);
		    Toast.makeText(this, "Copiado al portapapeles", Toast.LENGTH_SHORT).show();
		}else if(v.getId()==R.id.det_descargar){
			Uri webpage = Uri.parse(enlace);
			Intent web = new Intent(Intent.ACTION_VIEW, webpage);
			startActivity(web);
		}else if(v.getId()==R.id.img_oficial){
			if(oficial){
				Toast.makeText(this, "OFICIAL", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(this, "NO es oficial", Toast.LENGTH_SHORT).show();
			}
		}else if(v.getId()==R.id.img_tipo){
			Toast.makeText(this, strFormato, Toast.LENGTH_SHORT).show();
		}
	}
	
	/*
	 * S�lo lo utilizo para iniciar el Analytics.
	 * 
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}
	
	/*
	 * Igual para pararlo.
	 * 
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.

	}


}
